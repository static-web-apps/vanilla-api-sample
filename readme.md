# **GitLab Pipelines Sample: Azure Static Web Apps Deploy**

This sample uses the GitLab(static-web-apps) Azure Web Apps Deploy to deploy a Vanilla API app to [App Service Static Web Apps](https://docs.microsoft.com/en-us/azure/static-web-apps/).

[Azure Static Web Apps](https://docs.microsoft.com/en-us/azure/static-web-apps/overview) allows you to easily build Javascript apps in minutes. Use this repo with the [Add an API to Static Web Apps with Azure Functions](https://docs.microsoft.com/en-us/azure/static-web-apps/add-api?tabs=vanilla-javascript) article to build and customize a new static site. 

This repo is used as a starter for a very basic web application with an API.

**Prerequisites** 
- Active Azure account: If you don't have one, you can [create an account for free](https://azure.microsoft.com/en-us/free/).
- GitLab project: If you don't have one, you can [create a project for free](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project).
- GitLab includes Pipelines. If you need help getting started with Pipelines, see [create your first pipeline](https://docs.gitlab.com/ee/ci/quick_start/).

**Support**

This sample is provided "as is" and is not supported. Likewise, no commitments are made as to its longevity or maintenance.
